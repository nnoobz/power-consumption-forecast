
# coding: utf-8

# In[1]:


import pandas as pd
data = pd.read_csv("C:\SmartMeteringProject\DailySummaryAll\MeanMonthlyConsumptionAll.csv",index_col=0)
data.head()


# In[2]:


data.index = pd.to_datetime(data.index)


# In[3]:


data.columns = ['Energy Consumption']


# In[4]:


import plotly.plotly as ply
import cufflinks as cf
import plotly
plotly.tools.set_credentials_file(username='Dyrell', api_key='YzChA79a86xiPYVw1EHV')
data.iplot(title="Energy Consumption Nov 2011--Feb 2014")


# In[29]:


from pyramid.arima import auto_arima
stepwise_model = auto_arima(data, start_p=1, start_q=1,
                           max_p=3, max_q=3, m=12,
                           start_P=0, seasonal=True,
                           enforce_stationarity=False,
                           d=0, D=0, trace=True,
                           #enforce_stationarity=False,
                           error_action='ignore',  
                           suppress_warnings=True, 
                           stepwise=True)
print(stepwise_model.aic())


# In[30]:


train = data.loc['2011-11':'2013-12']
test = data.loc['2014-01':'2014-02']


# In[31]:


stepwise_model.fit(train)


# In[32]:


future_forecast = stepwise_model.predict(n_periods=2)
# This returns an array of predictions:
print(future_forecast)


# In[33]:


import plotly
plotly.tools.set_credentials_file(username='Dyrell', api_key='YzChA79a86xiPYVw1EHV')
future_forecast = pd.DataFrame(future_forecast,index = test.index,columns=["Prediction"])
pd.concat([test,future_forecast],axis=1).iplot()


# In[34]:


pd.concat([data,future_forecast],axis=1).iplot()

